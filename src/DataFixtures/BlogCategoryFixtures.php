<?php

namespace App\DataFixtures;

use App\Entity\BlogCategory;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BlogCategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $category1 = new BlogCategory();
        $category1->setCategory('Category1');
        $category1->setIsPublic(true);
        $manager->persist($category1);

        $category2 = new BlogCategory();
        $category2->setIsPublic(true);
        $category2->setCategory('Category2');
        $manager->persist($category2);

        $manager->flush();
    }
}

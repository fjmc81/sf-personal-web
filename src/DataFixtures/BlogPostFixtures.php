<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BlogPostFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $post1 = new BlogPost();
        $post1->setTitle('Post1');
        $post1->setAuthor('franky');
        $post1->setCreateAt(new \DateTimeImmutable("now"));
        $post1->setPost('
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed semper erat. Cras mollis efficitur gravida. In venenatis vestibulum enim non maximus. Phasellus vehicula justo ac purus pellentesque, nec pellentesque purus congue. Maecenas ut eleifend nisi, dictum condimentum arcu. Fusce non dui condimentum, tempor est eu, lacinia sapien. Etiam non est ac nisi tempor semper. Donec accumsan lectus eget quam placerat varius. Maecenas non viverra velit. Mauris auctor erat eget consectetur efficitur. Nunc rhoncus, erat eu gravida consequat, lorem quam viverra tellus, sed suscipit odio augue non nulla. Etiam pharetra efficitur efficitur. Etiam urna velit, bibendum ut gravida a, gravida eu ex.

        Duis aliquam metus id ultricies egestas. Aenean fringilla venenatis diam et scelerisque. Donec enim urna, pellentesque bibendum massa efficitur, scelerisque congue risus. Suspendisse potenti. Ut ullamcorper mi non quam ornare, at cursus nisi euismod. Etiam eros tortor, interdum ut velit et, dictum dapibus felis. In tempus neque vitae ultrices pharetra. Sed imperdiet sem nec purus elementum malesuada.

        Sed et consequat odio. Pellentesque id laoreet augue, eu bibendum justo. Vestibulum tincidunt faucibus ligula. Fusce egestas imperdiet purus, vel porttitor nulla pellentesque vitae. Proin facilisis mauris interdum urna sollicitudin, ut bibendum sem vulputate. Integer non semper justo, eu volutpat lacus. Proin ut odio posuere, condimentum metus eu, commodo arcu. Proin viverra porttitor neque, sed vulputate ligula sagittis sed. Donec ac eleifend nisl. Proin porttitor orci vitae cursus egestas. Aenean dignissim tellus sit amet nisi sollicitudin pulvinar. Donec quis odio malesuada, fringilla ligula in, consectetur massa. Suspendisse facilisis est lacus, et vestibulum turpis pretium a. Sed sollicitudin, justo id suscipit lacinia, lectus erat bibendum metus, id laoreet felis mauris eu nunc. Mauris eu libero ac quam ullamcorper mattis sed et turpis. Morbi ut imperdiet neque.
        ');
        //$post1->getTagid('tag1');
        $post1->setSlug('slug1');
        //$post1->getCatid();
        $post1->setIsPublic(true);
        $post1->setExcerpt('Donec molestie ullamcorper orci, nec faucibus quam lacinia feugiat. Aenean nec imperdiet enim. Aliquam malesuada dui eget tristique laoreet. Donec varius eu odio ac convallis. Pellentesque eu turpis sed risus porttitor mattis. Aliquam nulla tellus, pharetra quis pulvinar eget, consectetur eget ipsum.');
        $manager->persist($post1);

        $post2 = new BlogPost();
        $post2->setTitle('Post2');
        $post2->setAuthor('franky');
        $post2->setCreateAt(new \DateTimeImmutable("now"));
        $post2->setPost('
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed semper erat. Cras mollis efficitur gravida. In venenatis vestibulum enim non maximus. Phasellus vehicula justo ac purus pellentesque, nec pellentesque purus congue. Maecenas ut eleifend nisi, dictum condimentum arcu. Fusce non dui condimentum, tempor est eu, lacinia sapien. Etiam non est ac nisi tempor semper. Donec accumsan lectus eget quam placerat varius. Maecenas non viverra velit. Mauris auctor erat eget consectetur efficitur. Nunc rhoncus, erat eu gravida consequat, lorem quam viverra tellus, sed suscipit odio augue non nulla. Etiam pharetra efficitur efficitur. Etiam urna velit, bibendum ut gravida a, gravida eu ex.

        Duis aliquam metus id ultricies egestas. Aenean fringilla venenatis diam et scelerisque. Donec enim urna, pellentesque bibendum massa efficitur, scelerisque congue risus. Suspendisse potenti. Ut ullamcorper mi non quam ornare, at cursus nisi euismod. Etiam eros tortor, interdum ut velit et, dictum dapibus felis. In tempus neque vitae ultrices pharetra. Sed imperdiet sem nec purus elementum malesuada.

        Sed et consequat odio. Pellentesque id laoreet augue, eu bibendum justo. Vestibulum tincidunt faucibus ligula. Fusce egestas imperdiet purus, vel porttitor nulla pellentesque vitae. Proin facilisis mauris interdum urna sollicitudin, ut bibendum sem vulputate. Integer non semper justo, eu volutpat lacus. Proin ut odio posuere, condimentum metus eu, commodo arcu. Proin viverra porttitor neque, sed vulputate ligula sagittis sed. Donec ac eleifend nisl. Proin porttitor orci vitae cursus egestas. Aenean dignissim tellus sit amet nisi sollicitudin pulvinar. Donec quis odio malesuada, fringilla ligula in, consectetur massa. Suspendisse facilisis est lacus, et vestibulum turpis pretium a. Sed sollicitudin, justo id suscipit lacinia, lectus erat bibendum metus, id laoreet felis mauris eu nunc. Mauris eu libero ac quam ullamcorper mattis sed et turpis. Morbi ut imperdiet neque.
        ');
        //$post2->getTagid('tag2');
        $post2->setSlug('slug2');
        //$post2->getCatid();
        $post2->setIsPublic(true);
        $post2->setExcerpt('Donec molestie ullamcorper orci, nec faucibus quam lacinia feugiat. Aenean nec imperdiet enim. Aliquam malesuada dui eget tristique laoreet. Donec varius eu odio ac convallis. Pellentesque eu turpis sed risus porttitor mattis. Aliquam nulla tellus, pharetra quis pulvinar eget, consectetur eget ipsum.');
        $manager->persist($post2);

        $post3 = new BlogPost();
        $post3->setTitle('Post3');
        $post3->setAuthor('franky');
        $post3->setCreateAt(new \DateTimeImmutable("now"));
        $post3->setPost('
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed semper erat. Cras mollis efficitur gravida. In venenatis vestibulum enim non maximus. Phasellus vehicula justo ac purus pellentesque, nec pellentesque purus congue. Maecenas ut eleifend nisi, dictum condimentum arcu. Fusce non dui condimentum, tempor est eu, lacinia sapien. Etiam non est ac nisi tempor semper. Donec accumsan lectus eget quam placerat varius. Maecenas non viverra velit. Mauris auctor erat eget consectetur efficitur. Nunc rhoncus, erat eu gravida consequat, lorem quam viverra tellus, sed suscipit odio augue non nulla. Etiam pharetra efficitur efficitur. Etiam urna velit, bibendum ut gravida a, gravida eu ex.

        Duis aliquam metus id ultricies egestas. Aenean fringilla venenatis diam et scelerisque. Donec enim urna, pellentesque bibendum massa efficitur, scelerisque congue risus. Suspendisse potenti. Ut ullamcorper mi non quam ornare, at cursus nisi euismod. Etiam eros tortor, interdum ut velit et, dictum dapibus felis. In tempus neque vitae ultrices pharetra. Sed imperdiet sem nec purus elementum malesuada.

        Sed et consequat odio. Pellentesque id laoreet augue, eu bibendum justo. Vestibulum tincidunt faucibus ligula. Fusce egestas imperdiet purus, vel porttitor nulla pellentesque vitae. Proin facilisis mauris interdum urna sollicitudin, ut bibendum sem vulputate. Integer non semper justo, eu volutpat lacus. Proin ut odio posuere, condimentum metus eu, commodo arcu. Proin viverra porttitor neque, sed vulputate ligula sagittis sed. Donec ac eleifend nisl. Proin porttitor orci vitae cursus egestas. Aenean dignissim tellus sit amet nisi sollicitudin pulvinar. Donec quis odio malesuada, fringilla ligula in, consectetur massa. Suspendisse facilisis est lacus, et vestibulum turpis pretium a. Sed sollicitudin, justo id suscipit lacinia, lectus erat bibendum metus, id laoreet felis mauris eu nunc. Mauris eu libero ac quam ullamcorper mattis sed et turpis. Morbi ut imperdiet neque.
        ');
        //$post3->getTagid('tag1');
        $post3->setSlug('slug3');
        //$post3->getCatid();
        $post3->setIsPublic(true);
        $post3->setExcerpt('Donec molestie ullamcorper orci, nec faucibus quam lacinia feugiat. Aenean nec imperdiet enim. Aliquam malesuada dui eget tristique laoreet. Donec varius eu odio ac convallis. Pellentesque eu turpis sed risus porttitor mattis. Aliquam nulla tellus, pharetra quis pulvinar eget, consectetur eget ipsum.');
        $manager->persist($post3);

        $post4 = new BlogPost();
        $post4->setTitle('Post4');
        $post4->setAuthor('franky');
        $post4->setCreateAt(new \DateTimeImmutable("now"));
        $post4->setPost('
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed semper erat. Cras mollis efficitur gravida. In venenatis vestibulum enim non maximus. Phasellus vehicula justo ac purus pellentesque, nec pellentesque purus congue. Maecenas ut eleifend nisi, dictum condimentum arcu. Fusce non dui condimentum, tempor est eu, lacinia sapien. Etiam non est ac nisi tempor semper. Donec accumsan lectus eget quam placerat varius. Maecenas non viverra velit. Mauris auctor erat eget consectetur efficitur. Nunc rhoncus, erat eu gravida consequat, lorem quam viverra tellus, sed suscipit odio augue non nulla. Etiam pharetra efficitur efficitur. Etiam urna velit, bibendum ut gravida a, gravida eu ex.

        Duis aliquam metus id ultricies egestas. Aenean fringilla venenatis diam et scelerisque. Donec enim urna, pellentesque bibendum massa efficitur, scelerisque congue risus. Suspendisse potenti. Ut ullamcorper mi non quam ornare, at cursus nisi euismod. Etiam eros tortor, interdum ut velit et, dictum dapibus felis. In tempus neque vitae ultrices pharetra. Sed imperdiet sem nec purus elementum malesuada.

        Sed et consequat odio. Pellentesque id laoreet augue, eu bibendum justo. Vestibulum tincidunt faucibus ligula. Fusce egestas imperdiet purus, vel porttitor nulla pellentesque vitae. Proin facilisis mauris interdum urna sollicitudin, ut bibendum sem vulputate. Integer non semper justo, eu volutpat lacus. Proin ut odio posuere, condimentum metus eu, commodo arcu. Proin viverra porttitor neque, sed vulputate ligula sagittis sed. Donec ac eleifend nisl. Proin porttitor orci vitae cursus egestas. Aenean dignissim tellus sit amet nisi sollicitudin pulvinar. Donec quis odio malesuada, fringilla ligula in, consectetur massa. Suspendisse facilisis est lacus, et vestibulum turpis pretium a. Sed sollicitudin, justo id suscipit lacinia, lectus erat bibendum metus, id laoreet felis mauris eu nunc. Mauris eu libero ac quam ullamcorper mattis sed et turpis. Morbi ut imperdiet neque.
        ');
        //$post4->getTagid('tag1');
        $post4->setSlug('slug4');
        //$post4->getCatid();
        $post4->setIsPublic(true);
        $post4->setExcerpt('Donec molestie ullamcorper orci, nec faucibus quam lacinia feugiat. Aenean nec imperdiet enim. Aliquam malesuada dui eget tristique laoreet. Donec varius eu odio ac convallis. Pellentesque eu turpis sed risus porttitor mattis. Aliquam nulla tellus, pharetra quis pulvinar eget, consectetur eget ipsum.');
        $manager->persist($post4);

        $post5 = new BlogPost();
        $post5->setTitle('Post5');
        $post5->setAuthor('franky');
        $post5->setCreateAt(new \DateTimeImmutable("now"));
        $post5->setPost('
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sed semper erat. Cras mollis efficitur gravida. In venenatis vestibulum enim non maximus. Phasellus vehicula justo ac purus pellentesque, nec pellentesque purus congue. Maecenas ut eleifend nisi, dictum condimentum arcu. Fusce non dui condimentum, tempor est eu, lacinia sapien. Etiam non est ac nisi tempor semper. Donec accumsan lectus eget quam placerat varius. Maecenas non viverra velit. Mauris auctor erat eget consectetur efficitur. Nunc rhoncus, erat eu gravida consequat, lorem quam viverra tellus, sed suscipit odio augue non nulla. Etiam pharetra efficitur efficitur. Etiam urna velit, bibendum ut gravida a, gravida eu ex.

        Duis aliquam metus id ultricies egestas. Aenean fringilla venenatis diam et scelerisque. Donec enim urna, pellentesque bibendum massa efficitur, scelerisque congue risus. Suspendisse potenti. Ut ullamcorper mi non quam ornare, at cursus nisi euismod. Etiam eros tortor, interdum ut velit et, dictum dapibus felis. In tempus neque vitae ultrices pharetra. Sed imperdiet sem nec purus elementum malesuada.

        Sed et consequat odio. Pellentesque id laoreet augue, eu bibendum justo. Vestibulum tincidunt faucibus ligula. Fusce egestas imperdiet purus, vel porttitor nulla pellentesque vitae. Proin facilisis mauris interdum urna sollicitudin, ut bibendum sem vulputate. Integer non semper justo, eu volutpat lacus. Proin ut odio posuere, condimentum metus eu, commodo arcu. Proin viverra porttitor neque, sed vulputate ligula sagittis sed. Donec ac eleifend nisl. Proin porttitor orci vitae cursus egestas. Aenean dignissim tellus sit amet nisi sollicitudin pulvinar. Donec quis odio malesuada, fringilla ligula in, consectetur massa. Suspendisse facilisis est lacus, et vestibulum turpis pretium a. Sed sollicitudin, justo id suscipit lacinia, lectus erat bibendum metus, id laoreet felis mauris eu nunc. Mauris eu libero ac quam ullamcorper mattis sed et turpis. Morbi ut imperdiet neque.
        ');
        //$post5->getTagid('tag1');
        $post5->setSlug('slug5');
        //$post5->getCatid();
        $post5->setIsPublic(true);
        $post5->setExcerpt('Donec molestie ullamcorper orci, nec faucibus quam lacinia feugiat. Aenean nec imperdiet enim. Aliquam malesuada dui eget tristique laoreet. Donec varius eu odio ac convallis. Pellentesque eu turpis sed risus porttitor mattis. Aliquam nulla tellus, pharetra quis pulvinar eget, consectetur eget ipsum.');
        $manager->persist($post5);

        $manager->flush();
    }
}

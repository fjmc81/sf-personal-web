<?php

namespace App\DataFixtures;

use App\Entity\BlogTag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class BlogTagFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $tag1 = new BlogTag();
        $tag1->setTag('Tag1');
        $tag1->setIsPublic(true);
        $manager->persist($tag1);

        $tag2 = new BlogTag();
        $tag2->setTag('Tag2');
        $tag2->setIsPublic(true);
        $manager->persist($tag2);

        $tag3 = new BlogTag();
        $tag3->setTag('Tag3');
        $tag3->setIsPublic(true);
        $manager->persist($tag3);

        $tag4 = new BlogTag();
        $tag4->setTag('Tag4');
        $tag4->setIsPublic(true);
        $manager->persist($tag4);

        $manager->flush();
    }
}

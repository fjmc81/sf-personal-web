<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\BlogCategory;
use App\Entity\BlogTag;
use App\Repository\BlogPostRepository;
use App\Repository\BlogCategoryRepository;
use App\Repository\BlogTagRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

/**
 * @Route("/blog")
 */
class BlogController extends AbstractController
{

    /**
     * @Route("/", name="blog_index", methods={"GET"})
     */
    public function index(
        BlogTagRepository $BlogTagRepository, 
        BlogCategoryRepository $BlogCategoryRepository, 
        BlogPostRepository $BlogPostRepository,
        EntityManagerInterface $em,
        PaginatorInterface $paginator,
        Request $request
        ): Response
    {
        $dql = 
            'SELECT p
            FROM App\Entity\BlogPost p
            ORDER BY p.post ASC'
        ;
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate(
        $query, /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        5 /*limit per page*/
    );

        return $this->render('blog/index.html.twig', [
            //'blogs' => $BlogPostRepository->findLastestPosts(),
            'blogs' => $pagination,
            'menucats' => $BlogCategoryRepository->findAllCats(),
            'categories' => $BlogCategoryRepository->findAll(),
            'tags' => $BlogTagRepository->findAll()
            ]);
    }  

    /**
     * @Route("/{id}", name="blog_show", methods={"GET"})
     */
    public function blog_show(
        int $id, 
        BlogCategoryRepository $BlogCategoryRepository, 
        BlogTagRepository $BlogTagRepository, 
        BlogPostRepository $BlogPostRepository, 
        Request $request
        ): Response
    {
        $post = $this->getDoctrine()
            ->getRepository(BlogPost::class)
            ->find($id);
        // It's the same as doing find($id) on repository
        return $this->render('blog/show.html.twig', [
            'blog' => $BlogPostRepository->findOneBy(['id' => $post]),
            'menucats' => $BlogCategoryRepository->findAllCats(),
            'categories' => $BlogCategoryRepository->findAll(),
            'tags' => $BlogTagRepository->findAll()
            ]);
    }

    /**
     * @Route("/post/{id}", name="blog_by_id", requirements={"id"="\d+"}, methods={"GET"})
     * @ParamConverter("post", class="App:BlogPost")
     */
    public function post(
        BlogCategoryRepository $BlogCategoryRepository, 
        BlogTagRepository $BlogTagRepository, 
        BlogPostRepository $BlogPostRepository, 
        Request $request, 
        int $id
        ): Response
    {
        $post = $this->getDoctrine()
            ->getRepository(BlogPost::class)
            ->find($id);
        // It's the same as doing find($id) on repository
        return $this->render('blog/show.html.twig', [
            'blog' => $BlogPostRepository->findOneBy(['id' => $post]),
            'categories' => $BlogCategoryRepository->findAll(),
            'menucats' => $BlogCategoryRepository->findAllCats(),
            'tags' => $BlogTagRepository->findAll()]);
    }

    /**
     * @Route("/category/{id}", name="category_by_id", requirements={"id"="\d+"}, methods={"GET"})
     * @ParamConverter("category", class="App:BlogCategory")
     */
    public function category(
        BlogCategoryRepository $BlogCategoryRepository,
        BlogTagRepository $BlogTagRepository,
        int $id
        ): Response
    {
        $post = $this->getDoctrine()
            ->getRepository(BlogCategory::class)
            ->find($id);
        // It's the same as doing find($id) on repository
        return $this->render('blog/show_by_category.html.twig', [
            'menucats' => $BlogCategoryRepository->findAllCats(),
            'categories' => $BlogCategoryRepository->findAllCategories($id),
            'tags' => $BlogTagRepository->findAll()
            ]);
    }

    /**
     * @Route("/tag/{id}", name="tag_by_id", requirements={"id"="\d+"}, methods={"GET"})
     * @ParamConverter("tag", class="App:BlogTag")
     */
    public function tag(
        BlogCategoryRepository $BlogCategoryRepository,
        BlogTagRepository $BlogTagRepository, 
        int $id
        ): Response
    {
        $post = $this->getDoctrine()
            ->getRepository(BlogPost::class)
            ->find($id);
        // It's the same as doing find($id) on repository
        return $this->render('blog/show_by_tags.html.twig', [
            'menucats' => $BlogCategoryRepository->findAllCats(),
            'categories' => $BlogCategoryRepository->findAllCategories($id),
            'tags' => $BlogTagRepository->findAllTags($id)
            ]);
    }

    /**
     * @Route("/post/{slug}", name="blog_by_slug", methods={"GET"})
     * The below annotation is not required when $post is typehinted with BlogPost
     * and route parameter name matches any field on the BlogPost entity
     * @ParamConverter("BlogPost", class="App:BlogPost", options={"mapping": {"slug": "slug"}})
     */
    public function postBySlug(
        String $slug, 
        BlogCategoryRepository $BlogCategoryRepository, 
        BlogTagRepository $BlogTagRepository, 
        BlogPostRepository $BlogPostRepository, 
        Request $request
        ): Response
    {
        $post = $this->getDoctrine()
            ->getRepository(BlogPost::class)
            ->find($slug);
        // Same as doing findOneBy(['slug' => contents of {slug}])
        return $this->render('blog/show.html.twig', [
            'blog' => $BlogPostRepository->findOneBy(['slug' => $slug]),
            'menucats' => $BlogCategoryRepository->findAllCats(),
            'categories' => $BlogCategoryRepository->findAll(),
            'tags' => $BlogTagRepository->findAll()
            ]);
    }

    /**
     * @Route("/add", name="blog_add", methods={"POST"})
     */
    public function add(Request $request)
    {
        /** @var Serializer $serializer */
        $serializer = $this->get('serializer');

        $blogPost = $serializer->deserialize($request->getContent(), BlogPost::class, 'json');

        $em = $this->getDoctrine()->getManager();
        $em->persist($blogPost);
        $em->flush();

        return $this->json($blogPost);
    }

    /**
     * @Route("/post/{id}", name="blog_delete", methods={"DELETE"})
     */
    public function delete(BlogPost $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\BlogTag;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class BlogTagCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BlogTag::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm(),
            TextField::new('tag'),
            DateTimeField::new('created'),
            DateTimeField::new('updated'),
            BooleanField::new('isPublic'),
        ];
    }
}

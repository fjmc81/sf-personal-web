<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\BlogCategory;
use App\Entity\BlogTag;
use App\Repository\BlogPostRepository;
use App\Repository\BlogCategoryRepository;
use App\Repository\BlogTagRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("/", name="home")
 */
class HomeController extends AbstractController
{

    public function index(
        BlogTagRepository $BlogTagRepository, 
        BlogCategoryRepository $BlogCategoryRepository, 
        BlogPostRepository $BlogPostRepository,
        EntityManagerInterface $em,
        PaginatorInterface $paginator,
        Request $request
        ): Response
    {
        $dql = 
            'SELECT p
            FROM App\Entity\BlogPost p
            ORDER BY p.post ASC'
        ;
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate(
        $query, /* query NOT result */
        $request->query->getInt('page', 1), /*page number*/
        5 /*limit per page*/
        );
        return $this->render('landing_page/index.html.twig', [
            //'blogs' => $BlogPostRepository->findAll(),
            'blogs' => $pagination,
            'menucats' => $BlogCategoryRepository->findAllCats(),
            'categories' => $BlogCategoryRepository->findAll(),
            'tags' => $BlogTagRepository->findAll()
            ]);
    }
}

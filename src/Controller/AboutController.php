<?php

namespace App\Controller;

use App\Entity\BlogCategory;
use App\Entity\BlogTag;
use App\Repository\BlogCategoryRepository;
use App\Repository\BlogTagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    /**
     * @Route("/about", name="about")
     */
    public function index(
        BlogCategoryRepository $BlogCategoryRepository,
        BlogTagRepository $BlogTagRepository
    ): Response
    {
        return $this->render('about/index.html.twig', [
            'menucats' => $BlogCategoryRepository->findAllCats(),
            'categories' => $BlogCategoryRepository->findAll(),
            'tags' => $BlogTagRepository->findAll()
        ]);
    }
}

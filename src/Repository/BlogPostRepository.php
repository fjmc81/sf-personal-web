<?php

namespace App\Repository;

use App\Entity\BlogPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlogPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPost[]    findAll()
 * @method BlogPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlogPost::class);
    }

    public function findPostId($id){
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT u
            FROM AppEntityBlogPost u
            WHERE u.id :id'
        )->setParameter('id', $id);
        return $query->execute();
    }


    /*
    * @param string $query
    * @return mixed
    */
    public function findPostByName(String $query)
    {
        $qb = $this->createQueryBuilder('p');
        $qb
            ->where(
                $qb->expr()->andX(
                    $qb->expr()->like('p.title',':query'),
                    $qb->expr()->like('p.post',':query'),
                ),
                $qb->expr()->isNotNull('p.createAt'),
            )
            ->setParameter('query','%'.$query.'%')
        ;
        return $qb
            ->getQuery()
            ->getResult();
    }

    public function findLastestPosts()
    {
        return $this->createQueryBuilder('p')
            ->orderBy('p.createAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}

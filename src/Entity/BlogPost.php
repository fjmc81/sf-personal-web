<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use App\Repository\BlogPostRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=BlogPostRepository::class)
 */
class BlogPost
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     * @ORM\Column(type="string", length=255)
     */
    private $author;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     * @Assert\Length(min=20)
     */
    private $post;

    /**
     * @ORM\Column(type="datetime_immutable" , nullable=true)
     */
    private $createAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $slug;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsPublic;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $excerpt;

    /**
     * @ORM\ManyToOne(targetEntity=BlogCategory::class, inversedBy="blogPosts")
     */
    private $catid;

    /**
     * @ORM\ManyToMany(targetEntity=BlogTag::class, inversedBy="blogPosts")
     */
    private $tagid;
	
	public function __construct()
                            {
                                //if(!$this->setCreateAt()){
                                //    $this->setCreateAt(new \DateTimeImmutable());
                                //}
                                $this->setUpdated(new \DateTime());
                                $this->blogTag = new ArrayCollection();
                                $this->tagid = new ArrayCollection();
                            }
	
	public function __toString()
                            {
                                return (string) $this->getPost();
                            }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getPost(): ?string
    {
        return $this->post;
    }

    public function setPost(string $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeImmutable $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->IsPublic;
    }

    public function setIsPublic(bool $IsPublic): self
    {
        $this->IsPublic = $IsPublic;

        return $this;
    }

    public function getExcerpt(): ?string
    {
        return $this->excerpt;
    }

    public function setExcerpt(?string $excerpt): self
    {
        $this->excerpt = $excerpt;

        return $this;
    }

    public function getCatid(): ?BlogCategory
    {
        return $this->catid;
    }

    public function setCatid(?BlogCategory $catid): self
    {
        $this->catid = $catid;

        return $this;
    }

    /**
     * @return Collection|BlogTag[]
     */
    public function getTagid(): Collection
    {
        return $this->tagid;
    }

    public function addTagid(BlogTag $tagid): self
    {
        if (!$this->tagid->contains($tagid)) {
            $this->tagid[] = $tagid;
        }

        return $this;
    }

    public function removeTagid(BlogTag $tagid): self
    {
        $this->tagid->removeElement($tagid);

        return $this;
    }
}

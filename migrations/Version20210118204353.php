<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210118204353 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE blog_post_blog_tag (blog_post_id INT NOT NULL, blog_tag_id INT NOT NULL, INDEX IDX_CA877A44A77FBEAF (blog_post_id), INDEX IDX_CA877A442F9DC6D0 (blog_tag_id), PRIMARY KEY(blog_post_id, blog_tag_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE blog_post_blog_tag ADD CONSTRAINT FK_CA877A44A77FBEAF FOREIGN KEY (blog_post_id) REFERENCES blog_post (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blog_post_blog_tag ADD CONSTRAINT FK_CA877A442F9DC6D0 FOREIGN KEY (blog_tag_id) REFERENCES blog_tag (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE blog_comment');
        $this->addSql('ALTER TABLE blog_category ADD category VARCHAR(255) NOT NULL, ADD updated DATETIME NOT NULL, ADD created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD is_public TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE blog_post ADD category_id INT DEFAULT NULL, ADD title VARCHAR(255) NOT NULL, ADD author VARCHAR(255) NOT NULL, ADD post LONGTEXT NOT NULL, ADD create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD updated DATETIME DEFAULT NULL, ADD slug VARCHAR(255) DEFAULT NULL, ADD is_public TINYINT(1) NOT NULL, ADD excerpt LONGTEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE blog_post ADD CONSTRAINT FK_BA5AE01D12469DE2 FOREIGN KEY (category_id) REFERENCES blog_category (id)');
        $this->addSql('CREATE INDEX IDX_BA5AE01D12469DE2 ON blog_post (category_id)');
        $this->addSql('ALTER TABLE blog_tag ADD tag VARCHAR(255) NOT NULL, ADD created DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD updated DATETIME NOT NULL, ADD is_public TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE user ADD name VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE blog_comment (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE blog_post_blog_tag');
        $this->addSql('ALTER TABLE blog_category DROP category, DROP updated, DROP created, DROP is_public');
        $this->addSql('ALTER TABLE blog_post DROP FOREIGN KEY FK_BA5AE01D12469DE2');
        $this->addSql('DROP INDEX IDX_BA5AE01D12469DE2 ON blog_post');
        $this->addSql('ALTER TABLE blog_post DROP category_id, DROP title, DROP author, DROP post, DROP create_at, DROP updated, DROP slug, DROP is_public, DROP excerpt');
        $this->addSql('ALTER TABLE blog_tag DROP tag, DROP created, DROP updated, DROP is_public');
        $this->addSql('ALTER TABLE user DROP name');
    }
}
